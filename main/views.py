from django.shortcuts import render
from .forms import MatkulForm
from .models import MatkulModel
from django.shortcuts import redirect

def index(request):
    return render(request, 'main/index.html')

def intro(request):
    return render(request, 'main/intro.html')

def home(request):
    return render(request, 'main/home.html')

def about(request):
    return render(request, 'main/about.html')

def education(request):
    return render(request, 'main/education.html')

def experience(request):
    return render(request, 'main/experience.html')

def isimatkul(request):
    form = MatkulForm()

    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            post = form.save(commit=True)
            post.save()
            return redirect('/story5daftarmatkul')
    else:
        form = MatkulForm()

    return render(request, 'main/isimatkul.html', {'form' : form})

def daftarmatkul(request):
    daftar = MatkulModel.objects.all()
    context = { 'obj' : daftar }
    return render(request, 'main/daftarmatkul.html', context)

def hapus(request, pk):
    daftar = MatkulModel.objects.get(id=pk)
    if request.method == "POST":
        daftar.delete()
        return redirect('/story5daftarmatkul')
    
    context = {
        'item' : daftar
    }
    return render(request, 'main/hapus.html', context)