from django.db import models

# Create your models here.
TAHUN_AJAR = [
    ('2020/2021', '2020/2021 Gasal'),
    ('2020/2021', '2020/2021 Genap'),
]

class MatkulModel(models.Model):
    matkul = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.IntegerField()
    tahun = models.CharField(max_length=50, choices=TAHUN_AJAR)
    ruang = models.CharField(max_length=50)
    deskripsi = models.TextField()

    def __str__(self):
        return self.matkul

        
