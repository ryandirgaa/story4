from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('story1', views.intro, name='intro'),
    path('story3home', views.home, name='home'),
    path('story3about', views.about, name='about'),
    path('story3education', views.education, name='education'),
    path('story3experience', views.experience, name='experience'),
    path('story5isimatkul', views.isimatkul, name='isimatkul'),
    path('story5daftarmatkul', views.daftarmatkul, name='daftarmatkul'),
    path('story5hapus/<str:pk>', views.hapus, name='hapus')
]
