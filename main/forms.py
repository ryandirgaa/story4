from django import forms
from .models import MatkulModel

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatkulModel
        fields = '__all__'

        labels = {
            'matkul' : 'Matkul', 'dosen' : 'Dosen', 'sks' : 'SKS', 'tahun' : 'Tahun', 'ruang' : 'Ruang', 'deskripsi' : 'Deskripsi',
        }
