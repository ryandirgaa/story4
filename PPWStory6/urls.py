from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('isikegiatan', views.isikegiatan, name='isikegiatan'),
    path('daftarkegiatan', views.daftarkegiatan, name='daftarkegiatan'),
    path('peserta/<int:id>', views.peserta, name='peserta'),
]
