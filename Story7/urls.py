from django.urls import path

from . import views

app_name = 'Story7'

urlpatterns = [
    path('Story7/', views.home, name='home'),
]