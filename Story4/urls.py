"""Story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from Story8.views import data

urlpatterns = [
    path('admin/', admin.site.urls),
    path('Story8/data/',data),
    path('', include('main.urls')),
    path('', include('PPWStory6.urls', namespace='PPWStory6')),
    path('', include('Story7.urls', namespace='Story7')),
    path('', include('Story8.urls', namespace='Story8')),
    path('', include('Story9.urls', namespace='Story9')),
]
