from django.shortcuts import render
from django.http import JsonResponse
import json
from pip._vendor import requests

# Create your views here.
def home(request):
    response = {}
    return render(request, 'story8.html', response)

def data(request):
    argument = request.GET['q']
    url_destination = 'https://www.googleapis.com/books/v1/volumes?q=' + argument
    r = requests.get(url_destination)

    data2 = json.loads(r.content)
    return JsonResponse(data2, safe=False)