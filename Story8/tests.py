from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from Story8.apps import Story8Config
from selenium import webdriver
from django.urls import resolve, reverse
from .views import *
from .urls import *

# Create your tests here.

# TEST APP
class Story8ConfigTest(TestCase):
    def test_app(self):
        self.assertEqual(Story8Config.name, 'Story8')

# UNIT TEST
class Story8_Test(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_home(self):
        response = Client().get('/Story8/daftar')
        self.assertEqual(response.status_code, 200)

    def test_home_template(self):
        response = Client().get('/Story8/daftar')
        self.assertTemplateUsed(response, 'story8.html')

    def test_home_func(self):
        found = resolve('/Story8/daftar')
        self.assertEqual(found.func, home)
    
    def test_view_dalam_template_isikegiatan(self):
        response = Client().get('/Story8/daftar')  
        isi_view = response.content.decode('utf8')
        self.assertIn("Silakan temukan buku yang ingin Anda cari !", isi_view)   
        self.assertIn('<input id="find" placeholder="Judul Buku">', isi_view)


